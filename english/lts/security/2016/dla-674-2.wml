<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>The update for ghostscript issued as DLA-674-1 caused regressions for
certain Postscript document viewers (evince, zathura). Updated packages
are now available to address this problem. For reference, the original
advisory text follows.</p>

<p>Several vulnerabilities were discovered in Ghostscript, the GPL
PostScript/PDF interpreter, which may lead to the execution of arbitrary
code or information disclosure if a specially crafted Postscript file is
processed.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
9.05~dfsg-6.3+deb7u4.</p>

<p>We recommend that you upgrade your ghostscript packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

<p>--mP3DRpeJDSE+ciuQ
Content-Type: application/pgp-signature; name="signature.asc"
Content-Description: Digital signature</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-674-2.data"
# $Id: $
