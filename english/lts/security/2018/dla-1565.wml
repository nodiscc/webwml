<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security vulnerabilities were discovered in GlusterFS, a
clustered file system. Buffer overflows and path traversal issues may
lead to information disclosure, denial-of-service or the execution of
arbitrary code.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14651">CVE-2018-14651</a>

    <p>It was found that the fix for <a href="https://security-tracker.debian.org/tracker/CVE-2018-10927">CVE-2018-10927</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-10928">CVE-2018-10928</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-10929">CVE-2018-10929</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-10930">CVE-2018-10930</a>, and <a href="https://security-tracker.debian.org/tracker/CVE-2018-10926">CVE-2018-10926</a> was incomplete.
    A remote, authenticated attacker could use one of these flaws to
    execute arbitrary code, create arbitrary files, or cause denial of
    service on glusterfs server nodes via symlinks to relative paths.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14652">CVE-2018-14652</a>

    <p>The Gluster file system is vulnerable to a buffer overflow in the
    'features/index' translator via the code handling the
    <q>GF_XATTR_CLRLK_CMD</q> xattr in the <q>pl_getxattr</q> function. A remote
     authenticated attacker could exploit this on a mounted volume to
     cause a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14653">CVE-2018-14653</a>

    <p>The Gluster file system is vulnerable to a heap-based buffer
    overflow in the <q>__server_getspec</q> function via the <q>gf_getspec_req</q>
    RPC message. A remote authenticated attacker could exploit this to
    cause a denial of service or other potential unspecified impact.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14659">CVE-2018-14659</a>

    <p>The Gluster file system is vulnerable to a denial of service attack
    via use of the <q>GF_XATTR_IOSTATS_DUMP_KEY</q> xattr. A remote,
    authenticated attacker could exploit this by mounting a Gluster
    volume and repeatedly calling 'setxattr(2)' to trigger a state dump
    and create an arbitrary number of files in the server's runtime
    directory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14661">CVE-2018-14661</a>

    <p>It was found that usage of snprintf function in feature/locks
    translator of glusterfs server was vulnerable to a format string
    attack. A remote, authenticated attacker could use this flaw to
    cause remote denial of service.</p>


<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.5.2-2+deb8u5.</p>

<p>We recommend that you upgrade your glusterfs packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1565.data"
# $Id: $
