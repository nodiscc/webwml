<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several security issues have been corrected in multiple demuxers and
decoders of the libav multimedia library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-9317">CVE-2014-9317</a>

    <p>The decode_ihdr_chunk function in libavcodec/pngdec.c allowed remote
    attackers to cause a denial of service (out-of-bounds heap access)
    and possibly had other unspecified impact via an IDAT before an IHDR
    in a PNG file. The issue got addressed by checking IHDR/IDAT order.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-6761">CVE-2015-6761</a>

    <p>The update_dimensions function in libavcodec/vp8.c in libav relies on
    a coefficient-partition count during multi-threaded operation, which
    allowed remote attackers to cause a denial of service (race condition
    and memory corruption) or possibly have unspecified other impact via
    a crafted WebM file. This issue has been resolved by using
    num_coeff_partitions in thread/buffer setup. The variable is not a
    constant and can lead to race conditions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-6818">CVE-2015-6818</a>

    <p>The decode_ihdr_chunk function in libavcodec/pngdec.c did not enforce
    uniqueness of the IHDR (aka image header) chunk in a PNG image, which
    allowed remote attackers to cause a denial of service (out-of-bounds
    array access) or possibly have unspecified other impact via a crafted
    image with two or more of these chunks. This has now been fixed by
    only allowing one IHDR chunk. Multiple IHDR chunks are forbidden in
    PNG.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-6820">CVE-2015-6820</a>

    <p>The ff_sbr_apply function in libavcodec/aacsbr.c did not check for a
    matching AAC frame syntax element before proceeding with Spectral
    Band Replication calculations, which allowed remote attackers to
    cause a denial of service (out-of-bounds array access) or possibly
    have unspecified other impact via crafted AAC data. This has now been
    fixed by checking that the element type matches before applying SBR.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-6821">CVE-2015-6821</a>

    <p>The ff_mpv_common_init function in libavcodec/mpegvideo.c did not
    properly maintain the encoding context, which allowed remote
    attackers to cause a denial of service (invalid pointer access) or
    possibly have unspecified other impact via crafted MPEG data. The
    issue has been resolved by clearing pointers in ff_mpv_common_init().
    This ensures that no stale pointers leak through on any path.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-6822">CVE-2015-6822</a>

    <p>The destroy_buffers function in libavcodec/sanm.c did not properly
    maintain height and width values in the video context, which allowed
    remote attackers to cause a denial of service (segmentation violation
    and application crash) or possibly have unspecified other impact via
    crafted LucasArts Smush video data. The solution to this was to reset
    sizes in destroy_buffers() in avcodec/sanm.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-6823">CVE-2015-6823</a>

    <p>Other than stated in the debian/changelog file, this issue
    has not yet been fixed for libav in Debian jessie LTS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-6824">CVE-2015-6824</a>

    <p>Other than stated in the debian/changelog file, this issue
    has not yet been fixed for libav in Debian jessie LTS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-6825">CVE-2015-6825</a>

    <p>The ff_frame_thread_init function in libavcodec/pthread_frame.c
    mishandled certain memory-allocation failures, which allowed remote
    attackers to cause a denial of service (invalid pointer access) or
    possibly have unspecified other impact via a crafted file, as
    demonstrated by an AVI file. Clearing priv_data in
    avcodec/pthread_frame.c has resolved this and now avoids stale
    pointer in error case.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-6826">CVE-2015-6826</a>

    <p>The ff_rv34_decode_init_thread_copy function in libavcodec/rv34.c did
    not initialize certain structure members, which allowed remote
    attackers to cause a denial of service (invalid pointer access) or
    possibly have unspecified other impact via crafted (1) RV30 or (2)
    RV40 RealVideo data. This issue got addressed by clearing pointers in
    ff_rv34_decode_init_thread_copy() in avcodec/rv34.c, which avoids
    leaving stale pointers.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8216">CVE-2015-8216</a>

    <p>The ljpeg_decode_yuv_scan function in libavcodec/mjpegdec.c in FFmpeg
    omitted certain width and height checks, which allowed remote
    attackers to cause a denial of service (out-of-bounds array access)
    or possibly have unspecified other impact via crafted MJPEG data. The
    issues have been fixed by adding a check for index to
    avcodec/mjpegdec.c in ljpeg_decode_yuv_scan() before using it, which
    fixes an out of array access.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8217">CVE-2015-8217</a>

    <p>The ff_hevc_parse_sps function in libavcodec/hevc_ps.c did not
    validate the Chroma Format Indicator, which allowed remote attackers
    to cause a denial of service (out-of-bounds array access) or possibly
    have unspecified other impact via crafted High Efficiency Video
    Coding (HEVC) data. A check of chroma_format_idc in avcodec/hevc_ps.c
    has now been added to fix this out of array access.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8363">CVE-2015-8363</a>

    <p>The jpeg2000_read_main_headers function in libavcodec/jpeg2000dec.c
    did not enforce uniqueness of the SIZ marker in a JPEG 2000 image,
    which allowed remote attackers to cause a denial of service
    (out-of-bounds heap-memory access) or possibly have unspecified other
    impact via a crafted image with two or more of these markers. In
    avcodec/jpeg2000dec.c a check for duplicate SIZ marker has been added
    to fix this.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8364">CVE-2015-8364</a>

    <p>Integer overflow in the ff_ivi_init_planes function in
    libavcodec/ivi.c allowed remote attackers to cause a denial of
    service (out-of-bounds heap-memory access) or possibly have
    unspecified other impact via crafted image dimensions in Indeo Video
    Interactive data. A check of image dimensions has been added to the
    code (in avcodec/ivi.c) that fixes this integer overflow now.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8661">CVE-2015-8661</a>

    <p>The h264_slice_header_init function in libavcodec/h264_slice.c did
    not validate the relationship between the number of threads and the
    number of slices, which allowed remote attackers to cause a denial of
    service (out-of-bounds array access) or possibly have unspecified
    other impact via crafted H.264 data. In avcodec/h264_slice.c now
    max_contexts gets limited when slice_context_count is initialized.
    This avoids an out of array access.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8662">CVE-2015-8662</a>

    <p>The ff_dwt_decode function in libavcodec/jpeg2000dwt.c did not
    validate the number of decomposition levels before proceeding with
    Discrete Wavelet Transform decoding, which allowed remote attackers
    to cause a denial of service (out-of-bounds array access) or possibly
    have unspecified other impact via crafted JPEG 2000 data. In
    avcodec/jpeg2000dwt.c a check of ndeclevels has been added before
    calling dwt_decode*(). This fixes an out of array access.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8663">CVE-2015-8663</a>

    <p>The ff_get_buffer function in libavcodec/utils.c preserved width and
    height values after a failure, which allowed remote attackers to
    cause a denial of service (out-of-bounds array access) or possibly
    have unspecified other impact via a crafted .mov file. Now,
    dimensions get cleared in ff_get_buffer() on failure, which fixes
    the cause for an out of array access.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10190">CVE-2016-10190</a>

    <p>A heap-based buffer overflow in libavformat/http.c allowed remote web
    servers to execute arbitrary code via a negative chunk size in an
    HTTP response. In libavformat/http.c the length/offset-related
    variables have been made unsigned. This fix required inclusion of
    two other changes ported from ffmpeg upstream Git (commits 3668701f
    and 362c17e6).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10191">CVE-2016-10191</a>

    <p>Another heap-based buffer overflow in libavformat/rtmppkt.c allowed
    remote attackers to execute arbitrary code by leveraging failure to
    check for RTMP packet size mismatches. By checking for packet size
    mismatched, this out of array access has been resolved.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
6:11.12-1~deb8u2.</p>

<p>We recommend that you upgrade your libav packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1611.data"
# $Id: $
