<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p><a href="https://security-tracker.debian.org/tracker/CVE-2017-6307">CVE-2017-6307</a>
      An issue was discovered in tnef before 1.4.13. Two OOB Writes have
      been identified in src/mapi_attr.c:mapi_attr_read(). These might
      lead to invalid read and write operations, controlled by an attacker.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6308">CVE-2017-6308</a>

      <p>An issue was discovered in tnef before 1.4.13. Several Integer
      Overflows, which can lead to Heap Overflows, have been identified
      in the functions that wrap memory allocation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6309">CVE-2017-6309</a>

      <p>An issue was discovered in tnef before 1.4.13. Two type confusions
      have been identified in the parse_file() function. These might lead
      to invalid read and write operations, controlled by an attacker.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6310">CVE-2017-6310</a>

      <p>An issue was discovered in tnef before 1.4.13. Four type confusions
      have been identified in the file_add_mapi_attrs() function.
      These might lead to invalid read and write operations, controlled
      by an attacker.</p>


<p>
<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.4.9-1+deb7u1.</p>

<p>We recommend that you upgrade your tnef packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-839.data"
# $Id: $
