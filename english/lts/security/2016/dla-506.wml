<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabilities were discovered in dhcpcd5 a DHCP client package.
A remote (on a local network) attacker can possibly execute arbitrary
code or cause a denial of service attack by crafted messages.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-7912">CVE-2014-7912</a>

    <p>The get_option function does not validate the relationship between
    length fields and the amount of data, which allows remote DHCP
    servers to execute arbitrary code or cause a denial of service
    (memory corruption) via a large length value of an option in a
    DHCPACK message.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-7913">CVE-2014-7913</a>

    <p>The print_option function misinterprets the return value of the
    snprintf function, which allows remote DHCP servers to execute
    arbitrary code or cause a denial of service (memory corruption)
    via a crafted message.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
5.5.6-1+deb7u2.</p>

<p>We recommend that you upgrade your dhcpcd5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

 <p>--------------------- Ola Lundqvist /  opal@debian.org                     Folkebogatan 26          \
|  ola@inguza.com                      654 68 KARLSTAD          |
|  <a href="http://inguza.com/">http://inguza.com/</a>                  +46 (0)70-332 1551       |
\  gpg/f.p.: 22F2 32C6 B1E0 F4BF 2B26  0A6A 5E90 DCFA 9426 876F /
 </p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-506.data"
# $Id: $
