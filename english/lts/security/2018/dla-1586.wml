<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p><a href="https://security-tracker.debian.org/tracker/CVE-2018-0735">CVE-2018-0735</a>
      Samuel Weiser reported a timing vulnerability in the OpenSSL ECDSA
      signature generation, which might leak information to recover the
      private key.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5407">CVE-2018-5407</a>

      <p>Alejandro Cabrera Aldaya, Billy Brumley, Sohaib ul Hassan, Cesar
      Pereida Garcia and Nicola Tuveri reported a vulnerability to a
      timing side channel attack, which might be used to recover the
      private key.</p>


<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.0.1t-1+deb8u10.</p>

<p>We recommend that you upgrade your openssl packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1586.data"
# $Id: $
