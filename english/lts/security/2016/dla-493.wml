<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
  <p>* <a href="https://security-tracker.debian.org/tracker/CVE-2015-8312">CVE-2015-8312</a>:
    Off-by-one error in afs_pioctl.c in OpenAFS before 1.6.16 might allow
    local users to cause a denial of service (memory overwrite and system
    crash) via a pioctl with an input buffer size of 4096 bytes.</p>

  <p>* <a href="https://security-tracker.debian.org/tracker/CVE-2016-2860">CVE-2016-2860</a>:
    The newEntry function in ptserver/ptprocs.c in OpenAFS before 1.6.17
    allows remote authenticated users from foreign Kerberos realms to
    bypass intended access restrictions and create arbitrary groups as
    administrators by leveraging mishandling of the creator ID.</p>

  <p>* <a href="https://security-tracker.debian.org/tracker/CVE-2016-4536">CVE-2016-4536</a>:
    The client in OpenAFS before 1.6.17 does not properly initialize the
    (1) AFSStoreStatus, (2) AFSStoreVolumeStatus, (3) VldbListByAttributes,
    and (4) ListAddrByAttributes structures, which might allow remote
    attackers to obtain sensitive memory information by leveraging access
    to RPC call traffic.</p>


<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.6.1-3+deb7u6.</p>

<p>We recommend that you upgrade your openafs packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-493.data"
# $Id: $
