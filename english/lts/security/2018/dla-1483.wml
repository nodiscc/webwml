<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p><a href="https://security-tracker.debian.org/tracker/CVE-2018-10871">CVE-2018-10871</a></p>

    <p>By default nsslapd-unhashed-pw-switch was set to <q>on</q>. So a copy of
    the unhashed password was kept in modifiers and was possibly logged in
    changelog and retroCL.</p>

    <p>Unless it is used by some plugin it does not require to keep unhashed
    passwords. The nsslapd-unhashed-pw-switch option is now <q>off</q> by
    default.</p>

<p></p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10935">CVE-2018-10935</a>

    <p>It was discovered that any authenticated user doing a search using
    ldapsearch with extended controls for server side sorting could bring
    down the LDAP server itself.</p>

    <p>The fix is to check if we are able to index the provided value. If we
    are not, then slapd_qsort returns an error (LDAP_OPERATION_ERROR) .</p>


<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.3.3.5-4+deb8u2.</p>

<p>We recommend that you upgrade your 389-ds-base packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1483.data"
# $Id: $
