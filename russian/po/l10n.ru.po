# translation of l10n.ru.po to Russian
# Yuri Kozlov <kozlov.y@gmail.com>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml l10n\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2017-12-10 16:07+0500\n"
"Last-Translator: Lev Lamberov <dogsleg@debian.org>\n"
"Language-Team: Russian <debian-l10n-russian@lists.debian.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.0.4\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: ../../english/international/l10n/dtc.def:10
msgid "File"
msgstr "Файл"

#: ../../english/international/l10n/dtc.def:14
msgid "Package"
msgstr "Пакет"

#: ../../english/international/l10n/dtc.def:18
msgid "Score"
msgstr "Завершено"

#: ../../english/international/l10n/dtc.def:22
msgid "Translator"
msgstr "Переводчик"

#: ../../english/international/l10n/dtc.def:26
msgid "Team"
msgstr "Команда"

#: ../../english/international/l10n/dtc.def:30
msgid "Date"
msgstr "Дата"

#: ../../english/international/l10n/dtc.def:34
msgid "Status"
msgstr "Состояние"

#: ../../english/international/l10n/dtc.def:38
msgid "Strings"
msgstr "Строки"

#: ../../english/international/l10n/dtc.def:42
msgid "Bug"
msgstr "Сообщение об ошибке"

#: ../../english/international/l10n/dtc.def:49
msgid "<get-var lang />, as spoken in <get-var country />"
msgstr "<get-var lang />, страна - <get-var country />"

#: ../../english/international/l10n/dtc.def:54
msgid "Unknown language"
msgstr "Неизвестный язык"

#: ../../english/international/l10n/dtc.def:64
msgid "This page was generated with data collected on: <get-var date />."
msgstr "Эта страница создана по данным на: <get-var date />."

#: ../../english/international/l10n/dtc.def:69
msgid "Before working on these files, make sure they are up to date!"
msgstr "Перед началом работы над этими файлами убедитесь, что они актуальны!"

#: ../../english/international/l10n/dtc.def:79
msgid "Section: <get-var name />"
msgstr "Раздел: <get-var name />"

#: ../../english/international/l10n/menu.def:10
msgid "L10n"
msgstr "Локализация"

#: ../../english/international/l10n/menu.def:14
msgid "Language list"
msgstr "Список языков"

#: ../../english/international/l10n/menu.def:18
msgid "Ranking"
msgstr "Сравнение"

#: ../../english/international/l10n/menu.def:22
msgid "Hints"
msgstr "Советы"

#: ../../english/international/l10n/menu.def:26
msgid "Errors"
msgstr "Ошибки"

#: ../../english/international/l10n/menu.def:30
msgid "POT files"
msgstr "файлы .POT"

#: ../../english/international/l10n/menu.def:34
msgid "Hints for translators"
msgstr "Советы переводчикам"
