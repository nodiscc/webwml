<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been found in potrace.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2013-7437">CVE-2013-7437</a>

    <p>Multiple integer overflows in potrace 1.11 allow remote attackers
    to cause a denial of service (crash) via large dimensions in a BMP
    image, which triggers a buffer overflow.
    This bug was reported by Murray McAllister of the Red Hat
    Security Response Team.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8694">CVE-2016-8694</a>

<p><a href="https://security-tracker.debian.org/tracker/CVE-2016-8695">CVE-2016-8695</a>
<a href="https://security-tracker.debian.org/tracker/CVE-2016-8696">CVE-2016-8696</a></p>

    <p>Multiple NULL pointer dereferences in bm_readbody_bmp.
    This bug was discovered by Agostino Sarubbo of Gentoo.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8697">CVE-2016-8697</a>

    <p>Division by zero in bm_new.
    This bug was discovered by Agostino Sarubbo of Gentoo.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8698">CVE-2016-8698</a>

<p><a href="https://security-tracker.debian.org/tracker/CVE-2016-8699">CVE-2016-8699</a>
<a href="https://security-tracker.debian.org/tracker/CVE-2016-8700">CVE-2016-8700</a>
<a href="https://security-tracker.debian.org/tracker/CVE-2016-8701">CVE-2016-8701</a>
<a href="https://security-tracker.debian.org/tracker/CVE-2016-8702">CVE-2016-8702</a>
<a href="https://security-tracker.debian.org/tracker/CVE-2016-8703">CVE-2016-8703</a></p>

    <p>Multiple heap-based buffer overflows in bm_readbody_bmp.
    This bug was discovered by Agostino Sarubbo of Gentoo.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.10-1+deb7u1.</p>

<p>We recommend that you upgrade your potrace packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-675.data"
# $Id: $
