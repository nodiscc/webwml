<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>zutils version prior to version 1.8-pre2 contains a buffer
overflow vulnerability in zcat which happened with some
input files when the '-v, --show-nonprinting' option was
used (or indirectly enabled). This can result in potential
denial of service or arbitrary code execution. This attack
appear is exploitable via the victim openning a crafted
compressed file and has been fixed in 1.8-pre2.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in
version 1.3-4+deb8u1.</p>

<p>We recommend that you upgrade your zutils packages.</p>

<p>Further information about Debian LTS security advisories,
how to apply these updates to your system and frequently
asked questions can be found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1505.data"
# $Id: $
