<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Multiple vulnerabilities have been discovered in pdns, an authoritative
DNS server. The Common Vulnerabilities and Exposures project identifies
the following problems:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2120">CVE-2016-2120</a>

    <p>Mathieu Lafon discovered that pdns does not properly validate
    records in zones. An authorized user can take advantage of this flaw
    to crash server by inserting a specially crafted record in a zone
    under their control and then sending a DNS query for that record.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7068">CVE-2016-7068</a>

    <p>Florian Heinz and Martin Kluge reported that pdns parses all records
    present in a query regardless of whether they are needed or even
    legitimate, allowing a remote, unauthenticated attacker to cause an
    abnormal CPU usage load on the pdns server, resulting in a partial
    denial of service if the system becomes overloaded.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7072">CVE-2016-7072</a>

    <p>Mongo discovered that the webserver in pdns is susceptible to a
    denial-of-service vulnerability. A remote, unauthenticated attacker
    to cause a denial of service by opening a large number of f TCP
    connections to the web server.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7073">CVE-2016-7073</a>

<p>/ <a href="https://security-tracker.debian.org/tracker/CVE-2016-7074">CVE-2016-7074</a></p>

    <p>Mongo discovered that pdns does not sufficiently validate TSIG
    signatures, allowing an attacker in position of man-in-the-middle to
    alter the content of an AXFR.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.1-4.1+deb7u3.</p>

<p>We recommend that you upgrade your pdns packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

<p>Jonas Meurer</p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-798.data"
# $Id: $
