<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Security researchers identified two software analysis methods that, if
used for malicious purposes, have the potential to improperly gather
sensitive data from multiple types of computing devices with different
vendors’ processors and operating systems.</p>

<p>This update requires an update to the intel-microcode package, which
is non-free. Users who have already installed the version from
jessie-backports-sloppy do not need to upgrade.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-3639">CVE-2018-3639</a>

<p>– Speculative Store Bypass (SSB) – also known as Variant 4</p>

    <p>Systems with microprocessors utilizing speculative execution and
    speculative execution of memory reads before the addresses of all
    prior memory writes are known may allow unauthorized disclosure of
    information to an attacker with local user access via a side-channel
    analysis.</p>

<p></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-3640">CVE-2018-3640</a>

<p>– Rogue System Register Read (RSRE) – also known as
                Variant 3a</p>

    <p>Systems with microprocessors utilizing speculative execution and
    that perform speculative reads of system registers may allow
    unauthorized disclosure of system parameters to an attacker with
    local user access via a side-channel analysis.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.20180703.2~deb8u1.</p>

<p>We recommend that you upgrade your intel-microcode packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1446.data"
# $Id: $
